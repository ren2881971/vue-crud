const api = {
  login: '/login',
  index: '/index',
  regionManager: {
    list: '/regionManager/list'
  },
  refreshToken: '/refreshToken'
}
export default api
