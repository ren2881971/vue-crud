import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/api/api'
import qs from 'qs'
import axios from '@/utils/http'
import Storage from 'good-storage'
import { setToken, clearToken } from '@/utils/common'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    system: {
      hideSidebar: Storage.get('HideSidebar'),
      miniSidebar: Storage.get('MiniSidebar'),
      navType: parseInt(Storage.get('NavType') || 1)
    }
  },
  mutations: {
    LOGIN: (state, params) => {
      const accessToken = params.data.access_token
      const refreshToken = params.data.refresh_token
      clearToken()
      setToken(accessToken, refreshToken)
    },
    LOGOUT: (state) => {
      clearToken()
    },
    MINI_SIDEBAR_TOGGLE (state) {
      const miniSidebar = state.system.miniSidebar === 1 ? 0 : 1
      state.system.miniSidebar = miniSidebar
      Storage.set('MiniSidebar', miniSidebar)
    },
    HIDE_SIDEBAR_TOGGLE (state) {
      const hideSidebar = state.system.hideSidebar === 1 ? 0 : 1
      state.system.hideSidebar = hideSidebar
      Storage.set('HideSidebar', hideSidebar)
    },
    NAV_TYPE_TOGGLE (state, type) {
      state.system.navType = type
      Storage.set('NavType', type)
    }
  },
  actions: {
    login ({ commit }, params) {
      const username = params.username
      const password = params.password
      return new Promise((resolve, reject) => {
        axios.post(api.login, qs.stringify({
          username: username,
          password: password
        })).then((resp) => {
          if (resp.status === 200) {
            commit('LOGIN', resp.data)
            resolve(resp)
          }
        }).catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },
    logout ({ commit }) {
      commit('LOGOUT')
    },
    refreshToken ({ commit }, params) {
      const refreshToken = params.refreshToken
      clearToken()
      return new Promise((resolve, reject) => {
        axios.post(api.refreshToken, qs.stringify({
          refresh_token: refreshToken
        })).then((resp) => {
          if (resp.status === 200) {
            const json = resp.data
            if (json.code === 1) {
              commit('LOGIN', resp.data)
              resolve(resp)
            } else {
              commit('LOGOUT')
            }
          }
        }).catch(error => {
          commit('LOGOUT')
          reject(error)
        })
      })
    }
  },
  modules: {
  }
})
