import CommonConfig from '@/config/common'
import SystemConfig from '@/config/system'

export default {
  ...CommonConfig,
  ...SystemConfig,
  corporation: '长春吉大正元信息安全技术有限公司',
  siteName: 'JIT 版本升级管理平台'
}
