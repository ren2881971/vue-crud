CREATE TABLE `jit_region` (
  `region_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(100) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `is_parent` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='地区树形结构表'



CREATE TABLE `jit_server_resource` (
  `server_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `server_inner_ip` varchar(255) DEFAULT NULL,
  `server_name` varchar(255) DEFAULT NULL,
  `server_outer_ip` varchar(255) DEFAULT NULL,
  `server_type` int(11) NOT NULL DEFAULT 1,
  `server_use` text DEFAULT NULL,
  `region_Id` bigint(20) DEFAULT NULL,
  `has_ota` int(11) DEFAULT NULL,
  `has_cw` int(11) DEFAULT NULL,
  `ota_port` varchar(45) DEFAULT NULL,
  `cw_port` varchar(45) DEFAULT NULL,
  `common_date` datetime DEFAULT current_timestamp(),
  `region_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8