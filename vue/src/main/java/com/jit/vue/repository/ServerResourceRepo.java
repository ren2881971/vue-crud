package com.jit.vue.repository;

import com.jit.vue.pojo.ServerResource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServerResourceRepo extends JpaRepository<ServerResource,Long> {

    public ServerResource findByServerId(Long id);
}
