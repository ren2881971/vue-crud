package com.jit.vue.utils;

import com.jit.vue.exception.ExceptionEnum;

public class Result<T> {

    private Integer code;

    private String msg;

    private T data;
    private static final String SUCCESS_MESSAGE = "请求成功";
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    private Result(ExceptionEnum exceptionEnum) {
        this.code = exceptionEnum.getCode();
        this.msg = exceptionEnum.getMessage();
    }

    public static Result SUCCESS(Object t){
        Result result = new Result(1,SUCCESS_MESSAGE,t);
        return result;
    }

    public static Result ERROR(ExceptionEnum exceptionEnum){
        Result result = new Result(exceptionEnum);
        return result;
    }
}
