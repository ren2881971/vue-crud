package com.jit.vue.service;

import com.jit.vue.dto.RegionDto;
import com.jit.vue.dto.TreeNode;
import com.jit.vue.pojo.Region;

import java.util.List;
import java.util.Optional;

public interface RegionService {

    public List<TreeNode> findRegion();

    public boolean regionExist(Region region);

    public void addRegion(Region region);

    public void addParentRegion(Region region);

    public void deleteRegion(long id);

    public Optional<Region> findByRegionId(long id);

    public void updateRedion(Region region);
}
