package com.jit.vue.service;

import com.jit.vue.utils.JwtTokenDto;

public interface AuthService {

    public JwtTokenDto login(String username, String password);

    public JwtTokenDto refreshToken(String refreshToken);
}
