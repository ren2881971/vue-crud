package com.jit.vue.dto;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.List;

/*
Element-UI  Tree Node
 */
public class TreeNode {

    private long id;

    private String label;

    private List<TreeNode> children;

    private long parentId;

    private Boolean parent;

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public Boolean getParent() {
        return parent;
    }

    public void setParent(Boolean parent) {
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }
}
